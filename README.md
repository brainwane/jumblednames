I wrote this code to accompany [a review](http://www.harihareswara.net/sumana/2015/04/24/0) of *Hackster: The Revolution Begins*, a 2014 novel by Sankalp Kohli and Paritosh Yadav.

The code is under GPL; the list of names comes from [a blogroll by Teresa and Patrick Nielsen Hayden](http://nielsenhayden.com/antientblogroll.html).

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
