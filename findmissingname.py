#!/usr/bin/python3

# Copyright Sumana Harihareswara 2015; licensed GPL

# Compare two files and figure out what name got removed from one of them.

import codecs

def sift():
    # Get the archived list of names.
    with codecs.open("bloggers-archive.txt", encoding="utf-8") as f:
        originalnamelist = [line.strip('\n') for line in f]

    # Get the jumbled-up list that is missing one name.
    with codecs.open("bloggers-jumbled.txt", encoding="utf-8") as f:
        hackednamelist = [line.strip('\r\n') for line in f]

    # We'll be looking for the individual words within names.
    namewords = []
    for name in originalnamelist:
        name = name.strip()
        namewords += name.split(' ')

    for word in namewords:
        if word in hackednamelist:
            hackednamelist.remove(word)  # Remove matches
        else:
            print(word)  # Output namewords that aren't in the hacked list

if __name__ == '__main__':
    sift()
