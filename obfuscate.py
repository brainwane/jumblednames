#!/usr/bin/python3

# Copyright Sumana Harihareswara 2015; licensed GPL

# I removed one name from this list and am shuffling all the 
# names around to cover my tracks!

import codecs
with codecs.open("bloggers.txt", encoding="utf-8") as f:
    namelist = [line.strip('\n') for line in f]

# namelist is now ['John Adams ', 'John Joseph Adams ', 'Lou Anders ', ...]

# Let's get rid of that trailing space.
namelist = [item.strip() for item in namelist]

# If we really want to jumble things up, it'll help to pull all the names apart.

hackedlist = []
for item in namelist:
    hackedlist += item.split(' ')

# hackedlist is now ['John', 'Adams', 'John', 'Joseph', 'Adams', 'Lou', 'Anders', ...]

# Let's shuffle it.

import random
random.shuffle(hackedlist)
# hackedlist is now ['Bridesmaid', 'Richardson', 'Beth', 'Feruglio', 'Belle', ..]

# Write it back to a file.

with codecs.open("bloggers-jumbled.txt", encoding="utf-8", mode="w") as f:
    for item in hackedlist:
        f.write(item)
        f.write("\r\n")
